# TP 7 - Web Mobile

## Objectifs

  - Adapter votre application web pour une utilisation en contexte mobile
  - Accéder aux capteurs et actionneurs d'un smartphone
  - Terminer l'implémentation de votre jeu

## Pointeurs

  - [Les règles du jeu](https://forge.univ-lyon1.fr/m1if13/m1if13-2022/-/blob/main/pitch.md)
  - Capteurs
    - [Simuler sa localisation](https://developers.google.com/web/tools/chrome-devtools/device-mode/geolocation)
    - [Generic Sensor Demos](https://intel.github.io/generic-sensor-demos/) à tester avec différents dispositifs et navigateurs
  - Tester 
    - [Simulation mobile des Chrome Dev Tools](https://developers.google.com/web/tools/chrome-devtools/device-mode)
    - [Remote debugging toujours avec les Chrome Dev Tools](https://developers.google.com/web/tools/chrome-devtools/remote-debugging)
    - [Responsive Design Mode de Firefox](https://developer.mozilla.org/en-US/docs/Tools/Responsive_Design_Mode)

## Application

Dans ce TP, vous continuerez de travailler sur l'application VueJS des TPs précédents. Cet énoncé part du principe que le client et l'API côté serveur fonctionnent :

- Votre client est une application Vue.js destinée aux joueurs, avec un "store" pour la gestion des états et un routeur qui permet de gérer les différentes vues de votre application.
- Votre client est packagé en webpack et déployé sous la forme d'un ensemble de fichiers statiques sur nginx.
- Une route côté serveur permet d'accéder à l'API contenant le métier de l'application pour les joueurs "normaux".

## Travail à réaliser

### Capteurs et géolocalisation

**Première manipulation :**

&Agrave; l'aide de la [Geolocation API](https://www.w3.org/TR/geolocation-API/), créez une page avec un script simple (indépendant de votre applicaiton) qui récupère et affiche dans la console la position de l'utilisateur. Déployez ce script sur votre VM ou sur un serveur accessible de l'extérieur, et sortez pour vérifier que cela correspond bien à votre position.

**De retour chez vous :**

- mettez en place la récupération de la position dans l'application joueur de votre jeu
- faites patienter l'utilisateur tant que sa position n'est pas présente (affichez une erreur au bout d'une minute)
- remplacez la position mockée par celle récupérée depuis le GPS de l'appareil

Plutôt que d'interroger le GPS toutes les 5 secondes, découplez l'utilisation du capteur du reste de l'application à l'aide du store VueX :

- récupérez la position courante et utilisez [`watchPosition`](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/watchPosition) pour être notifié d'un changement de position
- à l'aide d'actions et de mutations, tenez le store à jour de la position de l'utilisateur
- utilisez les informations stockées dans le store pour mettre à jour la carte et envoyer la position au serveur
- vérifiez que les positions des différents joueurs s'affichent sur la carte

<div class="bleu">&Agrave; ce stade, votre application de jeu doit déjà être complètement fonctionnelle.</div>

### Vibreur

Une vue spéciale (modale) permettra d'afficher le fait que l'utilisateur local vient de gagner ou perdre la partie :

Faites [vibrer le téléphone](https://developer.mozilla.org/en-US/docs/Web/API/Vibration_API) en cas de fin de partie. 


### En bonus

Pour aller plus loin, vous pouvez implémenter une ou plusieurs des fonctionnalités suggérées ci-dessous.

#### Thème et luminosité

Compte tenu de l'importance de cette application pour les joueurs, il est important qu'ils puissent continuer à l'utiliser dans le noir. Vous allez donc :

- créer un second thème de l'application avec un fond plus sombre
- utiliser le capteur de [luminosité ambiante](https://www.w3.org/TR/ambient-light/) et définir une valeur seuil qui déclenchera le passage d'un thème à l'autre

**Aide** : pour cela il faut autoriser l'accès aux generic sensors : [chrome://flags/#enable-generic-sensor-extra-classes](chrome://flags/#enable-generic-sensor-extra-classes)

#### Direction des joueurs

Quand le joueur bouge, indiquer leur direction. Plusieurs approches sont possibles:

- calculer la direction en se basant sur la position courante et la position passée 
- utiliser la boussole (le magnétomètre) du téléphone. Le magnétomètre est protégé pour des raisons de protection de la vie privée. Il faut également activer l'[accès aux generic sensors](chrome://flags/#enable-generic-sensor-extra-classes) dans les options de Chrome.
- d'autres approches s'appuyant sur les valeurs de l'accéléromètre sont possibles.

## Déploiement

Buildez votre client Vue et déployez les fichiers générés à la racine de votre serveur nginx.
