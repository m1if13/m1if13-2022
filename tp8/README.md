# TP 8 - Progressive Web Apps

## Objectifs

Créer une Progressive Web 
  - Permettre une "installation" de l'application. 
  - Effectuer des notifications
  - Structurer un App Shell et le mettre en cache
  - Utiliser des Services Workers pour mettre en cache du contenu de manière dynamique


## Pointeurs

  - Progressive Web Apps :
    - [MDN Progressive Web Apps](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps)
    - [Web.dev Progressive Web Apps](https://web.dev/progressive-web-apps/)
    - [PWA Builder](https://www.pwabuilder.com/) ou tout autre [générateur de manifeste](https://www.google.com/search?q=Web+App+Manifest+Generator)
    - [Workbox pour la gestion des Service Workers](https://developer.chrome.com/docs/workbox/)
  - Tester 
    - [LightHouse](https://developers.google.com/web/tools/lighthouse)

## Application

Dans ce TP, vous continuerez de travailler sur l'application des TPs précédents. Cet énoncé part du principe que le client et l'API côté serveur fonctionnent :

- Votre client doit être packagé et déployé sur nginx.
- Votre client est une application Vue avec gestion des états et un routeur (côté client) qui permette de gérer les différentes vues de votre application.
- Une route côté serveur doit permettre d'accéder à l'API contenant le métier (actuel) de l'application.
- La partie mobile a été implémentée 

## Travail à réaliser

### Installation : Web App Manifest

Créez un fichier manifest qui permettent à votre application de "s'installer" sur mobile. 
**Il faudra spécifier à minima un icone, un url, et des éléments descriptifs.**

Vue.cli facilite la génération du fichier `manifest.json` normalement le plugin [cli-plugin-pwa](https://cli.vuejs.org/core-plugins/pwa.html) est installé par défaut. 

Il faut alors éditer l'entrée `pwa` dans `vue.config.js` (code repris de [la documentation](https://cli.vuejs.org/core-plugins/pwa.html#example-configuration)) :

```js
// Inside vue.config.js
module.exports = {
  // ...other vue-cli plugin options...
  pwa: {
    name: 'My App',
    themeColor: '#4DBA87',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
  }
}
```


Vous pouvez utiliser le panneau 'Application' des outils de développement Chrome pour inspecter le manifest.

Références :
  - [Spec W3C](https://www.w3.org/TR/appmanifest/)
  - [Doc MDN](https://developer.mozilla.org/en-US/docs/Web/Manifest)
  - [Doc Google](https://web.dev/add-manifest/)


### Notifications

Utiliser l'[API de notifications](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Re-engageable_Notifications_Push) pour notifier de la fin d'une partie.

**En option** : Vous pouvez l'API de push pour que le serveur signale à tous les participants via une notification les évènements majeurs de la partie (démarrage et fin). Pour gérer le push il sera nécessaire d'utiliser un service worker défini dans src/service-worker.js (ou ailleurs selon ce que vous avez défini dans vue.config.js)

### Mise en cache 

[Utiliser un Service Worker](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Offline_Service_workers) pour mettre en cache les fichiers du AppShell vu au 1e semestre: 
- Index et structure de l'application
- CSS
- images/logo

Au lieu de le faire "à la main", nous allons nous appuyer sur le [plugin workbox pour webpack](https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin) qui est utilisé par vue-cli.

```js
// Inside vue.config.js
module.exports = {
  // ...other vue-cli plugin options...
  pwa: {
    // ... other pwa options 

    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'src/sw.js'
      // ...other Workbox options...
    }
  }
}
```

Vous pouvez adapter [l'exemple présenté ici](https://gaute.app/dev-blog/workbox-in-vue) pour créer deux fichiers:

Dans `sw.js` on va définir des règles de caching. Voir [les recettes de workbox](https://developers.google.com/web/tools/workbox/guides/common-recipes) pour comprendre / adapter les règles :

```javascript
//This is how you can use the network first strategy for files ending with .js
workbox.routing.registerRoute(
  /.*\.js/,
  workbox.strategies.networkFirst()
)

// Use cache but update cache files in the background ASAP
workbox.routing.registerRoute(
  /.*\.css/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'css-cache'
  })
)

//Cache first, but defining duration and maximum files
workbox.routing.registerRoute(
  /.*\.(?:png|jpg|jpeg|svg|gif)/,
  workbox.strategies.cacheFirst({
    cacheName: 'image-cache',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 20,
        maxAgeSeconds: 7 * 24 * 60 * 60
      })
    ]
  })
)
```

Vous pourrez aussi trouver [ici une description de caching sans regex basée sur le type des contenus demandés](https://developer.chrome.com/docs/workbox/caching-resources-during-runtime/).

Dans `registerServiceWorker.js`

```javascript
 import { register } from 'register-service-worker'
 
 if (process.env.NODE_ENV === 'production') { // a adapter a votre configuration
  register(`${process.env.BASE_URL}sw.js`, {  // a adapter a votre configuration
    ready () {
      console.log(
        'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB'
      )
    },
    cached () {
      console.log('Content has been cached for offline use.')
    },
    updated () {
      console.log('New content is available; please refresh.')
    },
    offline () {
      console.log('No internet connection found. App is running in offline mode.')
    },
    error (error) {
      console.error('Error during service worker registration:', error)
    }
  })
}
```

Dans votre fichier principal vue, il faudra importer `registerServiceWorker` : 
```js
import './registerServiceWorker'
```

[Cet exemple de PWA Vue avec Workbox](https://github.com/pimhooghiemstra/plintpwa-vue-1) permet d'explorer un projet fonctionnel plus en détail.

&#x26a0; **Remarque :** Pour pouvoir utiliser le service worker de l'app (en HTTPS sur votre VM) depuis votre PC ou téléphone, vous devez installer le certificat racine.
- Sur PC, cela s'installe dans les paramètres du navigateur ; sur un téléphone, ça s'installe directement au niveau de l'OS.
- Pour Android : en s'envoyant le fichier par mail et en ouvrant la PJ, Android propose de l'installer directement.
- Pour IOS : suivre [ce lien](https://apple.stackexchange.com/questions/123988/how-to-add-a-crt-certificate-to-iphones-keychain).

Le certificat racine est dans le répertoire /Certificats depuis [la page d'accueil de l'UE](https://perso.liris.cnrs.fr/lionel.medini/enseignement/M1IF13/).

### Critères pour la soutenance
- une notification push pour la fin de la partie
- un manifeste proprement défini et permet d'installer l'application 
- une page offline est disponible. Voir [la recette Workbox associée](https://developer.chrome.com/docs/workbox/managing-fallback-responses/#offline-page-only)
